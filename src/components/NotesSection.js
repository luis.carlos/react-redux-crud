import React from "react";
import { useSelector, useDispatch } from "react-redux";
import NoteItem from "./NoteItem";
import inputActions from "../redux/actions/inputActions";
import "./NotesSection.style.scss";

const NotesSection = () => {
    const dispatch = useDispatch();
    const notes = useSelector((state) => state.notes.notes);

    const onItemClicked = (item, index) => {
        dispatch(inputActions.setInputId(index));
        dispatch(inputActions.setInputTitle(item.title));
        dispatch(inputActions.setInputContent(item.content));
    };

    if (notes.length === 0) {
        return (
            <div className="notesSection_container__empty">
                <p>Sem notas. Adicione alguma nota</p>
            </div>
        );
    }

    return (
        <div className="notesSection_container">
            {notes.map((item, index) => (
                <NoteItem
                    title={item.title}
                    content={item.content}
                    onItemClicked={() => {
                        onItemClicked(item, index);
                    }}
                />
            ))}
        </div>
    );
};

export default NotesSection;
