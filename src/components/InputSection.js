import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import noteActions from "../redux/actions/noteActions";
import inputActions from "../redux/actions/inputActions";
import "./InputSection.style.scss";

export default function InputSection() {
    const id = useSelector((state) => state.inputs.id);
    const title = useSelector((state) => state.inputs.title);
    const content = useSelector((state) => state.inputs.content);
    const dispatch = useDispatch();

    const addNote = () => {
        if (title && content) {
            dispatch(
                noteActions.addNote({
                    title,
                    content,
                })
            );
            dispatch(inputActions.resetInput());
        }
    };

    const updateNote = () => {
        if (title && content) {
            dispatch(
                noteActions.updateNote(id, {
                    title,
                    content,
                })
            );
            dispatch(inputActions.resetInput());
        }
    };
    const deleteNote = () => {};

    return (
        <div className="InputSection_container">
            <input
                type="text"
                placeholder="Titulo da Nota"
                value={title}
                onChange={(e) =>
                    dispatch(inputActions.setInputTitle(e.target.value))
                }
            />
            <textarea
                placeholder="Escreva a Nota"
                value={content}
                onChange={(e) =>
                    dispatch(inputActions.setInputContent(e.target.value))
                }
            ></textarea>
            <div className="InputSection_container__btnWrapper">
                <button onClick={id == -1 ? addNote : updateNote}>
                    {id === -1 ? "Adicionar" : "Atualizar"}
                </button>
                {id !== -1 && (
                    <button
                        onClick={deleteNote}
                        style={{ marginLeft: "1em", backgroundColor: "red" }}
                    >
                        Excluir Nota
                    </button>
                )}
            </div>
        </div>
    );
}
